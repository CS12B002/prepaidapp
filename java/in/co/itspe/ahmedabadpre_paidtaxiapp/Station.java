package in.co.itspe.ahmedabadpre_paidtaxiapp;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


public class Station extends ActionBarActivity {

    RadioGroup groupstation;
    Button bbackstation, bnextstation;
    RadioButton radio_b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station);
        OnclickListenerButton();

    }

    public void OnclickListenerButton() {
        groupstation = (RadioGroup) findViewById(R.id.groupstation);
        bnextstation = (Button) findViewById(R.id.bnextstation);
        bbackstation = (Button) findViewById(R.id.bbackstation);

        bnextstation.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int selected_id = groupstation.getCheckedRadioButtonId();
                        if(selected_id != -1){
                        radio_b = (RadioButton) findViewById(selected_id);
                        Toast.makeText(Station.this, radio_b.getText().toString(),Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Station.this, select_vehicle.class));
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Please select an option",Toast.LENGTH_LONG).show();
                        }
                    }
                }

        );
        bbackstation.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Station.this, Trip.class));
                    }
                }
        );
    }
}

