package in.co.itspe.ahmedabadpre_paidtaxiapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


public class Login extends ActionBarActivity implements View.OnClickListener {

    Button login;
    EditText username, password;
    TextView signup;
    ProgressDialog dialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.usernametextlogin);
        password = (EditText) findViewById(R.id.passwordtextlogin);
        login = (Button) findViewById(R.id.loginbtnlogin);
        signup = (TextView) findViewById(R.id.createaccounttextlogin);

        login.setOnClickListener(this);
        signup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.loginbtnlogin:{
                dialog = ProgressDialog.show(Login.this, "",
                        "Validating user...", true);
                new Thread(new Runnable() {
                    public void run() {
                        login();
                    }
                }).start();


            }

                break;

            case R.id.createaccounttextlogin:

                startActivity(new Intent(this, Register.class));

                break;
        }

    }


    void login(){
        try{

            URL url = new URL("http://52.26.219.136/prepaid_taxi/login.php?username=rajesh&password=rajesh");
            URLConnection urlConnection = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            String s = "";
            while ((line = reader.readLine()) != null) {
                s = s + line;
            }



            if(s.equalsIgnoreCase("1")){
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(Login.this, "Login Success", Toast.LENGTH_SHORT).show();
                    }
                });

                startActivity(new Intent(this, Trip.class));
            }else{
                showAlert();
            }

        }catch(Exception e){
            dialog.dismiss();
            System.out.println("Exception : " + e.getMessage());
        }
    }
    public void showAlert(){
        Login.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                builder.setTitle("Login Error.");
                builder.setMessage("User not Found.")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

}
