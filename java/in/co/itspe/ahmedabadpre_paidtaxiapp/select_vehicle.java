package in.co.itspe.ahmedabadpre_paidtaxiapp;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


public class select_vehicle extends ActionBarActivity {

    RadioGroup groupvehicle;
    RadioButton radio_b;
    Button bnextvehicle, bbackvehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_vehicle);
        OnclickListenerButton();

    }

    public void OnclickListenerButton() {
        groupvehicle = (RadioGroup) findViewById(R.id.groupvehicle);
        bnextvehicle = (Button) findViewById(R.id.bnextvehicle);
        bbackvehicle = (Button) findViewById(R.id.bbackvehicle);

        bnextvehicle.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int selected_id = groupvehicle.getCheckedRadioButtonId();
                        if(selected_id != -1){
                            radio_b = (RadioButton) findViewById(selected_id);
                            Toast.makeText(select_vehicle.this, radio_b.getText().toString(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(select_vehicle.this, Destination.class));
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Please select an option",Toast.LENGTH_LONG).show();
                        }
                    }
                }

        );
        bbackvehicle.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(select_vehicle.this, Station.class));
                    }
                }
        );
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_vehicle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
