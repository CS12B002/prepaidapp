package com.example.zz.prepaidapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;


public class Destination extends ActionBarActivity {

    Button bnextdest, bbackdest;
    AutoCompleteTextView desttext1,desttext2;
    private SQLiteCountryAssistant sqlliteCountryAssistant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);

        bnextdest = (Button) findViewById(R.id.bnextdest);
        bbackdest = (Button) findViewById(R.id.bbackdest);
        desttext1 = (AutoCompleteTextView) findViewById(R.id.desttext1);
        desttext2 = (AutoCompleteTextView) findViewById(R.id.desttext2);
        new Thread(new Runnable() {
            @Override
            public void run() {

                getcost();
            }
        }).start();

        String str1[]={"Habsiguda","Hitec"};
        String str2[]={"Panjagutta","Patrika Nagar"};



        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);

        //arrayAdapter.add("Habsiguda");
        arrayAdapter.add("Hitec");
        arrayAdapter1.add("Panjagutta");
        //arrayAdapter1.add("Patrika Nagar");

        SQLiteDatabase db = openOrCreateDatabase("taxi", MODE_PRIVATE, null);
        String queryend1 = "SELECT end1 from costs group by end1";
        Cursor resultSet1 = db.rawQuery(queryend1,null);



        resultSet1.moveToFirst();
        //Toast.makeText(Destination.this,resultSet1.getString(0),Toast.LENGTH_LONG).show();
        do {
            arrayAdapter.add(resultSet1.getString(0));
        } while (resultSet1.moveToNext());



        resultSet1.close();

        String queryend2 = "SELECT end2 from costs group by end2";
        Cursor resultSet2 = db.rawQuery(queryend2,null);
        resultSet2.moveToFirst();
        do {
            arrayAdapter1.add(resultSet2.getString(0));
        } while (resultSet2.moveToNext());
        resultSet2.close();

        desttext1.setThreshold(1);
        desttext1.setAdapter(arrayAdapter);

        desttext1.setThreshold(1);
        desttext2.setAdapter(arrayAdapter1);



        bnextdest.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Login.global.put("destination1",desttext1.getText());
                            Login.global.put("destination2",desttext2.getText());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent launchNextActivity;
                        launchNextActivity = new Intent(Destination.this, Cost.class);
                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(launchNextActivity);

                    }
                }
        );
        bbackdest.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent launchNextActivity;
                        launchNextActivity = new Intent(Destination.this, Select_vehicle.class);
                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(launchNextActivity);
                    }
                }
        );

    }
    void getcost(){
        try {
            URL url = new URL("http://52.26.219.136/prepaid_taxi/getcost.php");

            URLConnection urlConnection = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            String s = "";
            while ((line = reader.readLine()) != null) {

                s = s + line;
            }
            SQLiteDatabase db = openOrCreateDatabase("taxi", MODE_PRIVATE, null);

            db.execSQL("DROP TABLE IF EXISTS costs;");
            db.execSQL("CREATE TABLE IF NOT EXISTS costs(id INT, start VARCHAR,end1 VARCHAR,end2 VARCHAR,cost VARCHAR);");

            JSONObject jo = new JSONObject(s);
            JSONArray jsonArray = jo.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject dummy = jsonArray.getJSONObject(i);
                String query = "INSERT INTO costs VALUES('" + dummy.getInt("id") + "'," + "'" + dummy.getString("start") + "'," + "'" + dummy.getString("end1") + "'," + "'" + dummy.getString("end2") +"',"+"'"+dummy.getString("cost")+ "');";
                //names.add(dummy.getString("name"));
                db.execSQL(query);



            }
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(Destination.this,e.toString(), Toast.LENGTH_LONG).show();
                }
            });

        }
    }
    @Override
    public void onBackPressed() {
        Login.global.remove("destination1");
        Login.global.remove("destination2");
        Intent launchNextActivity;
        launchNextActivity = new Intent(Destination.this, Select_vehicle.class);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(launchNextActivity);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_destination, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
