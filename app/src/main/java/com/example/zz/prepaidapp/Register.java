package com.example.zz.prepaidapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Register extends ActionBarActivity implements View.OnClickListener {

    Button registerbtnregister;
    EditText nametextregister, phonetextregister, usernametextregister, passwordtextregister, passwordrepeattextregister;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nametextregister = (EditText) findViewById(R.id.nametextregister);
        phonetextregister = (EditText) findViewById(R.id.phonetextregister);
        usernametextregister = (EditText) findViewById(R.id.usernametextregister);
        passwordtextregister = (EditText) findViewById(R.id.passwordtextregister);
        passwordrepeattextregister = (EditText) findViewById(R.id.passwordrepeattextregister);
        registerbtnregister = (Button) findViewById(R.id.registerbtnregister);

        registerbtnregister.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registerbtnregister:
                startActivity(new Intent(this, Station.class));


                break;
        }
    }
}
