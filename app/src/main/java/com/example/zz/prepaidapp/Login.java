package com.example.zz.prepaidapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


public class Login extends ActionBarActivity implements View.OnClickListener {

    Button login;
    EditText username, password;
    TextView signup;
    ProgressDialog dialog = null;
    public String user_name;
    public String pass_word;

    public static JSONObject global = new JSONObject();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.
                ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.usernametextlogin);
        password = (EditText) findViewById(R.id.passwordtextlogin);
        login = (Button) findViewById(R.id.loginbtnlogin);
        signup = (TextView) findViewById(R.id.createaccounttextlogin);


        login.setOnClickListener(this);
        signup.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.loginbtnlogin:
            {


                //dialog = ProgressDialog.show(Login.this, "",
                   //     "Validating user...", true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            login();
                        }
                    }).start();


            }






                break;

            case R.id.createaccounttextlogin:

                startActivity(new Intent(this, Register.class));

                break;
        }

    }


    void login(){
        try{
             try {
                    global.put("username",username.getText());
                    global.put("password",password.getText());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            /*runOnUiThread(new Runnable() {
                              public void run() {
                                  Toast.makeText(Login.this, "1" + username.getText(), Toast.LENGTH_SHORT).show();

                              }
                          });*/
            URL url = new URL("http://52.26.219.136/prepaid_taxi/login.php?username="+username.getText()+"&password="+password.getText());
            URLConnection urlConnection = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            String s = "";
            while ((line = reader.readLine()) != null) {

                s = s + line;
            }



            if(s.equalsIgnoreCase("1")){

                runOnUiThread(new Runnable() {
                    public void run() {

                            Toast.makeText(Login.this,"username", Toast.LENGTH_SHORT).show();
                    }
                });
                Intent launchNextActivity;
                launchNextActivity = new Intent(Login.this, Trip.class);
                launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(launchNextActivity);

                    }



            else{

                showAlert();
            }

        }catch(final Exception e){
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(Login.this,e.toString(), Toast.LENGTH_LONG).show();
                }
            });
            dialog.dismiss();

            System.out.println("Exception : " + e.getMessage());
        }
    }
    public void showAlert(){
        Login.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                builder.setTitle("Login Error.");
                builder.setMessage("User not Found.")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }


}
