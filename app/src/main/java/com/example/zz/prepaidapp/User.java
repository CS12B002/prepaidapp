package com.example.zz.prepaidapp;

/**
 * Created by Admin on 13-07-2015.
 */
public class User {
    String name, username, password, repeatpassword;
    int phonenumber;

    public User (String name, int phonenumber, String username , String password, String repeatpassword){
        this.name = name;
        this.phonenumber = phonenumber;
        this.username = username;
        this.password = password;
        this.repeatpassword = repeatpassword;
    }

    public User (String username, String password) {
        this.username = username;
        this.password = password;
        this.phonenumber = -1;
        this.name = "";
        this.repeatpassword = "";


    }
}
