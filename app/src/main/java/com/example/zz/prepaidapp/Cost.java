package com.example.zz.prepaidapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Debug;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


public class Cost extends ActionBarActivity {

    TextView costtext,fromtext,totext;
    Button bnextcost, bbackcost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cost);



        bnextcost = (Button) findViewById(R.id.bnextcost);
        bbackcost = (Button) findViewById(R.id.bbackcost);
        fromtext = (TextView)findViewById(R.id.tripstart);
        totext = (TextView)findViewById(R.id.tripdest);
        costtext = (TextView) findViewById(R.id.triprate);
        try {
            fromtext.setText(Login.global.getString("station"));
            totext.setText(Login.global.getString("destination1") + "," + Login.global.getString("destination2"));
            SQLiteDatabase db = openOrCreateDatabase("taxi", MODE_PRIVATE, null);
            String query = "SELECT cost from costs WHERE start = '"+Login.global.getString("station")+"'  AND end1 = '"+Login.global.getString("destination1")+"' AND end2 = '"+Login.global.getString("destination2")+"';";
            Cursor resultSet = db.rawQuery(query, null);
            resultSet.moveToFirst();
            costtext.setText(resultSet.getString(0)+"/-");
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(Cost.this,e.toString(), Toast.LENGTH_LONG).show();
                }
            });
            e.printStackTrace();
        }
        bnextcost.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Cost.this, Instructions.class));
                    }
                }
        );
        bbackcost.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Cost.this, Destination.class));
                    }
                }
        );


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cost, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
